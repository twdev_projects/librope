#!/bin/bash

meson compile -C bld

./bld/tests/rope_bench \
    --benchmark_format=csv \
    --benchmark_filter=BM_rope_concatenation \
    >rope.csv

./bld/tests/rope_bench \
    --benchmark_format=csv \
    --benchmark_filter=BM_stringstream_concatenation \
    >streams.csv

gnuplot -p tests/benchmark.gnuplot
