#include <gtest/gtest.h>

#include <rope/rope.h>


class RopeTest : public ::testing::Test {
public:
    void SetUp() {
    }

    void TearDown() {
    }

protected:
};


class RopeConcatTest : public ::testing::TestWithParam<std::size_t> {
public:
    void SetUp() {
    }

    void TearDown() {
    }

protected:
};



TEST_F(RopeTest, test_trivialSizeCalculation) {
    std::string s = "this is a rope with only one leaf node";
    rope::rope r{s};
    ASSERT_EQ(s.size(), r.size());
}


TEST_F(RopeTest, test_trivialStringComparison) {
    std::string s = "this is a rope with only one leaf node";
    rope::rope r{s};
    ASSERT_EQ(s, r.to_string());
}


TEST_F(RopeTest, test_ifStringRemainsCorrectAfterSimpleConcatenation) {
    std::string s = "initial";
    std::string extra = "extra string to be concatenated";

    std::stringstream ss;
    ss << s;
    ss << extra;

    rope::rope r{s};
    r.append(rope::rope{extra});

    const auto expected = ss.str();
    const auto have = r.to_string();
    ASSERT_EQ(expected, have);
    ASSERT_EQ(expected.size(), r.size());
    ASSERT_EQ(expected.size(), have.size());
}


TEST_P(RopeConcatTest, test_ifStringRemainsCorrectAfterInvolvedConcatenation) {
    std::string s = "initial";

    const std::size_t numConcats = GetParam();
    const std::size_t chunkLen = 256;

    std::stringstream ss;
    ss << s;

    rope::rope r{s};

    for (std::size_t i = 0; i < numConcats; ++i) {
        std::string chunk(chunkLen, 'a');
        ss << chunk;
        r.append(rope::rope{std::move(chunk)});
    }

    const auto expected = ss.str();
    const auto have = r.to_string();
    ASSERT_EQ(expected, have);
    ASSERT_EQ(expected.size(), r.size());
    ASSERT_EQ(expected.size(), have.size());
}

INSTANTIATE_TEST_CASE_P(
        RopeConcatTest,
        RopeConcatTest,
        ::testing::Values(
            128,
            1024,
            8192,
            32768,
            1000000));
