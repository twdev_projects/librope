#include <benchmark/benchmark.h>

#include <rope/rope.h>

#include <iostream>
#include <sstream>

static void BM_rope_concatenation(benchmark::State& state) {
    rope::rope r{"I'm the initial string"};
    const std::size_t cnt = state.range(0);

    for (auto _ : state) {
        state.PauseTiming();
        std::string s(cnt, 'a');
        state.ResumeTiming();
        r.append(std::move(s));
    }
}

static void BM_stringstream_concatenation(benchmark::State& state) {
    std::stringstream ss;
    ss << "I'm the initial string";
    const std::size_t cnt = state.range(0);

    for (auto _ : state) {
        state.PauseTiming();
        std::string s(cnt, 'a');
        state.ResumeTiming();
        ss << s;
    }
}


BENCHMARK(BM_rope_concatenation)
    ->Iterations(10000)
    ->RangeMultiplier(4)
    ->Range(256, 1 << 20);

BENCHMARK(BM_stringstream_concatenation)
    ->Iterations(10000)
    ->RangeMultiplier(4)
    ->Range(256, 1 << 20);

BENCHMARK_MAIN();
