set title 'String concatenation'
set ylabel 'time [ms]'
set xlabel 'chunk size [kB]'

set grid ytics mytics
set mytics 8
set grid

set key autotitle columnhead
set datafile separator ','

show grid

set y2tics nomirror
set ytics 0,600

plot 'rope.csv' using 0:3 with lp title "Rope" axis x1y1, \
         'streams.csv' using 0:3 with lp title "Streams" axis x1y2
