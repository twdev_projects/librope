# librope

This small library implements the [rope]() data structure in its
simplest form.  The code is not optimised and is meant to be used
for experimental purposes only.

Read more at [twdev.blog]() to learn more details.

## Building

    meson setup bld
    meson compile -C bld

