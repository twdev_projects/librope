#ifndef ROPE_H_
#define ROPE_H_

#include <memory>
#include <string>

namespace rope
{
    class node;

    /**
     * @class rope
     * @brief Represents a rope data structure
     *
     */
    class rope {
    public:
        ~rope();

        /**
         * @brief Constructs rope from a given string
         *
         * @param s initial string to store in rope
         */
        explicit rope(std::string s);

        /**
         * @brief Constructs rope from a given node
         *
         * @param root node defining a root
         */
        explicit rope(std::unique_ptr<node> root);

        rope(const rope&) = delete;

        rope(rope&& o);

        rope& operator=(rope o);

        /**
         * @brief Returns number of characters within the rope
         */
        std::size_t size() const;

        /**
         * @brief Converts the rope to string
         */
        std::string to_string() const;

        /**
         * @brief Returns the character at position
         *
         * Throw std::out_of_range if given offset >= size().
         *
         * @param offs position
         */
        std::string::value_type at(std::size_t offs) const;

        /**
         * @brief Appends given rope
         *
         * Given rope is moved
         *
         * @param rhs 
         */
        void append(rope rhs);

        /**
         * @brief Appends given string to rope
         *
         * @param s 
         */
        void append(std::string s);

        /**
         * @brief Splits rope at position
         *
         * All characters to the left of the provided split position will become
         * part of the returned rope.  Characters from pos >= offs will remain
         * in this rope.
         *
         * @param offs split position
         * @return
         */
        rope split_at(std::size_t offs);

    private:
        std::unique_ptr<node> root;
    };
} /* namespace rope */

std::ostream& operator<<(std::ostream& os, const rope::rope& r);

#endif /* ROPE_H_ */
