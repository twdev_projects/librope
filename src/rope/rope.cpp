#include <rope/rope.h>

#include <stdexcept>
#include <vector>

namespace rope
{
    class node {
    public:
        virtual ~node() = default;

        /**
         * @brief Returns number of characters within left subtree
         */
        virtual std::size_t weight() const = 0;

        /**
         * @brief Returns number of characters within this node (or its left + right subtrees)
         */
        virtual std::size_t size() const = 0;

        /**
         * @brief Serialises node contents to string
         */
        virtual std::string to_string() const = 0;

        /**
         * @brief Retrieves character at position
         */
        virtual std::string::value_type at(std::size_t) const = 0;

        /**
         * @brief Splits the node at given position
         *
         * After splitting, the current node becomes invalid.
         *
         * @param offs position at which the split will be performed
         */
        virtual std::pair<std::unique_ptr<node>, std::unique_ptr<node>> split_at(std::size_t offs) = 0;
    };


    class internal : public node {
    public:
        ~internal() {
            std::vector<std::unique_ptr<node>> nodes;
            std::vector<std::unique_ptr<node>> to_visit;

            to_visit.push_back(std::move(left));
            to_visit.push_back(std::move(right));

            for (; to_visit.size() > 0; ) {
                auto nod = std::move(to_visit.back());
                to_visit.pop_back();

                if (auto n = dynamic_cast<internal*>(nod.get()); n != nullptr) {
                    if (n->left) {
                        to_visit.push_back(std::move(n->left));
                    }

                    if (n->right) {
                        to_visit.push_back(std::move(n->right));
                    }

                    nodes.push_back(std::move(nod));
                } else {
                    // it's a leaf, just add it to the list
                    nodes.push_back(std::move(nod));
                }
            }
        }

        explicit internal(std::unique_ptr<node> left,
                std::unique_ptr<node> right) :
            left{std::move(left)},
            right{std::move(right)}
        {
        }

        std::size_t weight() const override {
            return left->size();
        }

        std::size_t size() const override {
            std::size_t s = 0;
            std::vector<node*> nodes;
            nodes.push_back(left.get());
            nodes.push_back(right.get());
            while (nodes.size() > 0) {
                auto n = nodes.back();
                nodes.pop_back();
                if (auto nc = dynamic_cast<internal*>(n); nc != nullptr) {
                    if (nc->left) {
                        nodes.push_back(nc->left.get());
                    }

                    if (nc->right) {
                        nodes.push_back(nc->right.get());
                    }
                } else {
                    s += n->size();
                }
            }
            return s;
        }

        std::string to_string() const override {
            return to_string(left.get()) + to_string(right.get());
        }

        std::string::value_type at(std::size_t offs) const override {
            if (const auto w = weight(); offs < w) {
                return left->at(offs);
            } else {
                return right->at(offs - w);
            }

            // should never get here
            return 0;
        }

        std::pair<std::unique_ptr<node>, std::unique_ptr<node>>
            split_at(std::size_t offs) override {
                if (const auto leftSize = left->size(); offs == leftSize) {
                    return std::make_pair(std::move(left), std::move(right));
                } else if (offs < leftSize) {
                    auto [l1, l2] = left->split_at(offs);
                    return std::make_pair(std::move(l1),
                            std::make_unique<internal>(std::move(l2), std::move(right)));
                } else {
                    auto [r1, r2] = right->split_at(offs - leftSize);
                    return std::make_pair(
                            std::make_unique<internal>(std::move(left), std::move(r1)),
                            std::move(r2));
                }
            }

    private:
        std::unique_ptr<node> left;
        std::unique_ptr<node> right;

        static std::string to_string(node* curr) {
            std::string output;
            std::vector<node*> nodes;

            if (curr == nullptr) {
                return "";
            }

            while (nodes.size() > 0 || curr != nullptr) {
                if (curr != nullptr) {
                    nodes.push_back(curr);
                    if (auto n = dynamic_cast<internal*>(curr); n != nullptr) {
                        curr = n->left.get();
                    } else {
                        curr = nullptr;
                    }
                } else {
                    curr = nodes.back();
                    nodes.pop_back();
                    if (auto n = dynamic_cast<internal*>(curr); n != nullptr) {
                        curr = n->right.get();
                    } else {
                        output.append(curr->to_string());
                        curr = nullptr;
                    }
                }
            }

            return output;
        }

    };


    class leaf : public node {
    public:
        explicit leaf(std::string s) :
            str{std::move(s)}
        {
        }

        std::size_t weight() const override {
            return size();
        }

        std::size_t size() const override {
            return str.length();
        }

        std::string to_string() const override {
            return str;
        }

        std::string::value_type at(std::size_t offs) const override {
            if (offs < size()) {
                return str[offs];
            }

            throw std::out_of_range("index bigger than length");
        }

        std::pair<std::unique_ptr<node>, std::unique_ptr<node>>
            split_at(std::size_t offs) override {
                auto l1 = std::make_unique<leaf>(str.substr(0, offs));
                auto l2 = std::make_unique<leaf>(str.substr(offs));
                return std::make_pair(std::move(l1), std::move(l2));
            }

    private:
        const std::string str;
    };


    rope::~rope()
    {
    }

    rope::rope(std::string s) :
        root{std::make_unique<leaf>(std::move(s))}
    {
    }

    rope::rope(std::unique_ptr<node> root) :
        root{std::move(root)}
    {
    }

    rope::rope(rope&& o) :
        root{std::move(o.root)}
    {
    }

    rope& rope::operator=(rope o) {
        std::swap(o.root, root);
        return *this;
    }

    std::size_t rope::size() const {
        return root->size();
    }

    std::string rope::to_string() const {
        return root->to_string();
    }

    std::string::value_type rope::at(std::size_t offs) const {
        return root->at(offs);
    }

    void rope::append(rope rhs) {
        root = std::make_unique<internal>(
                std::move(root),
                std::move(rhs.root));
    }

    void rope::append(std::string s) {
        root = std::make_unique<internal>(
                std::move(root),
                std::make_unique<leaf>(std::move(s)));
    }

    rope rope::split_at(std::size_t offs) {
        auto [r1, r2] = root->split_at(offs);
        root = std::move(r2);
        return rope{std::move(r1)};
    }

} /* namespace rope */


std::ostream& operator<<(std::ostream& os, const rope::rope& r) {
    os << r.to_string();
    return os;
}
